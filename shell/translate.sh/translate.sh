#!/bin/bash

## NOOBLY WRITTEN

while getopts "f:t:w:" opt; do
  case $opt in
    f) f=$OPTARG ;;
    t) l=$OPTARG;;
    w) str=$(echo $OPTARG | sed 's/ /+/g') ;;
    *) printf "Available flags are [-f], [-t] & [-w].\n" && exit ;;
  esac
done

file="/tmp/meaning.html"

curl -s "https://simplytranslate.org/?engine=google&sl=$f&tl=$l&text=$str" | pup > $file
echo "Translation:" $(pup -f $file 'textarea.translation text{}')
