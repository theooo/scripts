# nightcore-generator

![nightcoregen](./nightcoregen.png)

Generate nightcore tracks from YT via FFmpeg, all in one go.

## Why does this exist in the first place?

While lying gnu/bored-ass on tha couch listenin' to nightcore music, I suddenly had this idea:<br>
Is it possible to create nightcore tracks via bash utilizing ffmpeg & youtube-dl?

Turns out, I was ```science-damn``` right.

## Prerequisites

1. FFmpeg (obviously)
2. The legendary [ytfzf](https://github.com/pystardust/ytfzf)

## Installation

Just move nightcoregen into ```/usr/bin``` or $PATH. (muh lazy problems)

## Usage

There ain't no flags and stuff, just execute the script and follow the instructions.
