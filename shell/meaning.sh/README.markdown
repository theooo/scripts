## meaning.sh

a simple shell script to look up the meaning of a word from [Free Dictionary API](https://dictionaryapi.dev/).  
requires [jq](https://github.com/stedolan/jq).
