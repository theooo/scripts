#!/usr/bin/python3

from fractions import Fraction as frac
from sys import argv

def helpinfo():
    print('''
aspectratiocalc.py

Usage: aspectratiocalc.py [width] [height]

Options:
  -h, --help      show this help message and exit''')

if '-h' in argv or '--help' in argv:
    helpinfo()
else:
    try:
        ratio = str(frac(int(argv[1]), int(argv[2])))
    except IndexError:
        print("Input two values.")
    else:
        print(ratio.replace('/', ':'))
