# aspectratiocalc.py

a simple python script to calculate the aspect ratio of a given resolution.

## USAGE
```
aspectratiocalc.py

Usage: aspectratiocalc.py [width] [height]

Options:
  -h, --help      show this help message and exit
```
