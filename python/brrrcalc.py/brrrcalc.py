#!/usr/bin/python3

import argparse, time

desc = "brrrcalc.py.\nfind out how long it would take to watch your cs50 courses in 1.5x.\nnoobly written + done for the lulz."

parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-t', '--timestamp', dest='tstamp', metavar='', type=str, help="timestamp in HH:MM:SS")
parser.add_argument('-s', '--speed', dest='speed', metavar='', type=float, help="playback speed")
args = parser.parse_args()

if not args.speed: print("Playback speed not provided.")
if not args.tstamp: print("Timestamp not provided.")

hh, mm, ss = args.tstamp.split(':')
output = (int(hh)*3600 + int(mm)*60 + int(ss))

print('Time:', time.strftime('%H:%M:%S', time.gmtime(output/args.speed)), end="\t\t")
print('Difference:', time.strftime('%H:%M:%S', time.gmtime(output-(output/args.speed))))
