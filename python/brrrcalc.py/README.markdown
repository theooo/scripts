# brrrcalc.py

a simple python script to calculate the time taken for a video after increasing the playback speed.  

## USAGE
```
usage: brrrcalc.py [-h] [-t] [-s]

brrrcalc.py.
find out how long it would take to watch your cs50 courses in 1.5x.
noobly written + done for the lulz.

options:
  -h, --help         show this help message and exit
  -t , --timestamp   timestamp in HH:MM:SS
  -s , --speed       playback speed
```
