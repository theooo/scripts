#!/bin/python3

from sys import argv
from datetime import date

def help():
    print("""
datecalc.py date-1 [date-2] [-h/--help]

calculate the no. of days between two given dates or a given date and today."

POSITIONAL ARGUMENTS:
- date-1 == the date in YYYY-MM-DD format.

OPTIONAL POSITIONAL ARGUMENTS:
- date-2 == the date in YYYY-MM-DD format.
            if not provided, today's date will be used.

FLAGS:
- -h/ --help == display this text and exit.
""")

def calc():
    delta = abs(D1 - D0)
    print(delta.days)

if __name__ == '__main__':
    if len(argv) >= 4:
        print("max three values.")
        help()

    elif len(argv) == 3:
        try:
            D0 = date.fromisoformat(argv[1])
            D1 = date.fromisoformat(argv[2])
        except:
            print("Invalid dates.")
        else: calc()

    elif len(argv) == 2:
        if argv[1] == "-h" or argv[1] == "--help":
            help()
        else:
            try:
                D0 = date.fromisoformat(argv[1])
                D1 = date.today()
            except:
                print("Invalid date.")
            else: calc()

    elif (len(argv)) == 1:
        help()
