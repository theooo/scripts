# datecalc.py

a simple python script to calculate the no. of days between two given dates or a given date and today.

## USAGE
```
usage: datecalc.py [-h] [-f] [-l]

options:
  -h, --help  show this help message and exit
  -f          earliest date in YYYY-MM-DD.
  -l          latest date in YYYY-MM-DD. if not provided, today's date would
              be used.
```
